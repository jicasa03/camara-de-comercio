import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'dart:io';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import './principal.dart';
import './registrar.dart';
import './conexion/servidor.dart';
import './ui/notificacion.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:provider/provider.dart';
import 'package:sign_button/sign_button.dart';
import './provider/Notificaciones.dart';
import 'package:flutter_awesome_buttons/flutter_awesome_buttons.dart';
import 'package:url_launcher/url_launcher.dart';
void main() => runApp(   MultiProvider(
  providers: [
    ChangeNotifierProvider(create: (_) => Counter()),
  ],
  child: MyApp(),
),);

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Camara de comercio',
      theme: ThemeData(

        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'CAMARA DE COMERCIO'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {


  @override
  void initState() {
    super.initState();
    firebaseCloudMessaging_Listeners();
  }
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
 String boton="INICIAR SESIÓN";
bool estado=true;
String token1="";
  void firebaseCloudMessaging_Listeners() {
    if (Platform.isIOS) iOS_Permission();

    _firebaseMessaging.getToken().then((token){
      print(token);
      token1=token;
    });

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');
      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },
    );
  }

  void iOS_Permission() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true)
    );
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings)
    {
      print("Settings registered: $settings");
    });
  }
  var _scaffoldKey = new GlobalKey<ScaffoldState>();
void login() async{
   Notificacion notificar=new Notificacion();
   final prefs = await SharedPreferences.getInstance();
    Map request={};

    if (_fbKey.currentState.saveAndValidate()) {

    setState(() {
      boton="CARGANDO...";
      estado=false;
    });
    request=_fbKey.currentState.value;
    //request["clave"]="123";

    Map response={};
    response =await Conexion().enviar_servidor("login", request);
    print(response);
     if(response["estado"]==1){

       Map resquest1={};
       resquest1["codasociado"]=response["codasociado"];
       prefs.setString("id_usuario", resquest1["codasociado"]);
       resquest1["token"]=token1;
       var response1 =await Conexion().enviar_servidor("actualizar_token", resquest1);
       request["codasociado"]=resquest1["codasociado"];
      Map responses = await Conexion().enviar_servidor("perfil", request);
       prefs.setString("nombre", responses["nombres"]);
    //  print(response1);

       Navigator.pushReplacement(
         context,
         MaterialPageRoute(
           builder: (context) => Principal(),
           // Pass the arguments as part of the RouteSettings. The
           // ExtractArgumentScreen reads the arguments from these
           // settings.

         ),
       );
     }else{
   print("errror");
  /* Navigator.pushReplacement(
     context,
     MaterialPageRoute(
       builder: (context) => Principal(),
       // Pass the arguments as part of the RouteSettings. The
       // ExtractArgumentScreen reads the arguments from these
       // settings.

     ),
   );*/
   notificar.showInSnackBar_error("ERROR AL INCIAR SESION", context, _scaffoldKey);
   setState(() {
     boton="INICIAR SESIÓN";
     estado=true;
   });
     }


    }
}


  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      key: _scaffoldKey,

      appBar: AppBar(
        backgroundColor: Colors.green,
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text("CAMARA DE COMERCIO"),
      ),
      body:Container(
      child:SingleChildScrollView(
      child: Column(
        children: <Widget>[
      FormBuilder(
       key: _fbKey,
        child: Column(

          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              height: 80,
            ),
           Container(
             height: 130,
             width: 300,
             child:  Image.asset("assets/logo.png"),
           ),
           Padding(
             child:  FormBuilderTextField(
               attribute: "usuario",
               validators: [
                 FormBuilderValidators.required(
                   errorText:
                   "Se requiere usuario",
                 ),
               ],
               decoration: InputDecoration(labelText: "Ingresar Usuario",
                 contentPadding: EdgeInsets.fromLTRB(10, 20, 10, 20),
                 border: OutlineInputBorder(),
                 prefixIcon: Icon(Icons.person),
               ),


             ),
             padding: EdgeInsets.fromLTRB(10, 40, 10, 0),
           ),
          Padding(
            child: FormBuilderTextField(
              obscureText: true,
              attribute: "clave",
              validators: [
              FormBuilderValidators.required(
              errorText:
              "Se requiere clave",
            ),
              ],
              decoration: InputDecoration(labelText: "Ingresar Clave",
                contentPadding: EdgeInsets.fromLTRB(10, 20, 10, 20),
                prefixIcon: Icon(Icons.security),

                border: OutlineInputBorder(),
              ),

            ),
            padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
          ),
            
            Padding(
              padding: EdgeInsets.fromLTRB(10, 20, 10, 0),
              child: RaisedButton(onPressed:estado?login:null,
              color: Colors.green
              ,child: SizedBox(
                  width: MediaQuery.of(context).size.height,
                  height: 50,
                  child: Align(
                    child: Text("$boton",style: TextStyle(
                      color: Colors.white,
                      fontSize: 18
                    ),),
                    alignment: Alignment.center,

                  )

                ),
              ),
            ),
            Container(
              height: 20,
            ),
     Padding(padding: EdgeInsets.fromLTRB(20, 0, 20, 0)
     ,child:   Row(
         mainAxisAlignment: MainAxisAlignment.spaceBetween,
         children: <Widget>[
           SignInButton.mini(

             buttonType: ButtonType.twitter,
             onPressed: () {
              /* Navigator.pushReplacement(
                 context,
                 MaterialPageRoute(
                   builder: (context) => Registrar(),
                   // Pass the arguments as part of the RouteSettings. The
                   // ExtractArgumentScreen reads the arguments from these
                   // settings.

                 ),
               );*/

               _launchURL('https://twitter.com/CCPTSM');
             },
           ),
           SignInButton.mini(

             buttonType: ButtonType.facebook,
             onPressed: () {
               _launchURL('https://www.facebook.com/Camaratarapoto');
             },
           ),
           InstagramButton(onPressed: (){
             _launchURL('https://www.instagram.com/camaratarapoto/?hl=es-la');
           },),

           /*    SignInButton.mini(
             buttonType: ButtonType.,
             onPressed: () {},
           ),*/
           SignInButton.mini(

             buttonType: ButtonType.linkedin,
             onPressed: () {
               _launchURL('https://www.linkedin.com/in/c%C3%A1mara-de-comercio-tarapoto-ccptsmt-4a0813a9/');
             },
           ),
         ],
       ),
     )

          ],
        ),
        )
          ])
      )
      )

    );
  }

  _launchURL(String url) async {
    //const url = 'https://flutter.dev';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
