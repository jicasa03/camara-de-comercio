import 'package:camara/conexion/servidor.dart';
import 'package:camara/nuevo_servicio.dart';
import 'package:flutter/material.dart';
class Servicio extends StatefulWidget {
  Servicio({Key key, this.title}) : super(key: key);

  final String title;

  ServicioState createState() => ServicioState();
}

class ServicioState extends State<Servicio> {
  
  void initState()
  {
    super.initState();
    this.cargar();
    print("prueba");
  }
  List response=[];
  void cargar() async
  {
    Conexion conexion=new Conexion();
    Map request={};
    response= await conexion.enviar_servidor("servicios", request);
   response.forEach((element) {
     print(element);
   });

   setState(() {

   });
  }
  Widget build(BuildContext context) {
        return Scaffold(
          appBar: AppBar(
            title: Text("Servicio"),
            backgroundColor: Colors.green,

          ),
          body:
          Container(
            child: SingleChildScrollView(

              child: ListView.builder(
                  itemCount: response.length,
                  shrinkWrap: true,
                  itemBuilder: (BuildContext cont,int inde){
                return

                  GestureDetector(
                    onTap: (){
                      print("dadas");


                    },
                    child:Padding(padding: EdgeInsets.fromLTRB(10, 2, 10, 2),
                  child: OutlineButton(onPressed: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Nuevo_servicio(data:response[inde]),
                        // Pass the arguments as part of the RouteSettings. The
                        // ExtractArgumentScreen reads the arguments from these
                        // settings.

                      ),
                    );
                  },child: SizedBox(
                    width: MediaQuery.of(context).size.width,
                    height: 30,
                    child: Align(
                      child: Text(response[inde]["titulo"]),
                    ),
                  ),)
                  ,
                ));
              }),
            ),
          )

        );
  }
}

