class Cursos{
  final String id;
  final String  titulo;
  final String descripcion;
  final String foto;
  final String fechainicio;
  final String tipo;
  final int asistencia;
  final String fechafin;
  final String horainicio;
  final String horafin;
  final String costo;
  final String zoom;

  Cursos({this.id,this.titulo,this.descripcion,this.foto,this.fechainicio,this.tipo,this.asistencia,this.horainicio
  ,this.costo,this.fechafin,this.horafin,
    this.zoom
  });
}