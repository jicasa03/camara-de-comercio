import 'package:flutter/material.dart';
import '../complemento/color.dart';
class Notificacion  {


  void showInSnackBar_error(String value,BuildContext context, _scaffoldKey) {
    FocusScope.of(context).requestFocus(new FocusNode());
    _scaffoldKey.currentState?.removeCurrentSnackBar();
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(
        value,
        textAlign: TextAlign.justify,
        style: TextStyle(
            color: Colors.white,
            fontSize: 16.0,
            fontFamily: "Helvetica"),
      ),
      backgroundColor: Colors.red,
      duration: Duration(seconds: 3),
    ));
  }

  void showInSnackBar_correcto(String value,BuildContext context, _scaffoldKey) {
    FocusScope.of(context).requestFocus(new FocusNode());
    _scaffoldKey.currentState?.removeCurrentSnackBar();
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(
        value,
        textAlign: TextAlign.justify,
        style: TextStyle(
            color: Colors.white,
            fontSize: 16.0,
            fontFamily: "Helvetica"),
      ),
      backgroundColor:HexColor("#0d47a1"),
      duration: Duration(seconds: 3),
    ));
  }
}