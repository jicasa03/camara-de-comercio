import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import './conexion/servidor.dart';
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import './ui/notificacion.dart';
class Editar_perfil extends StatefulWidget {
  Editar_perfil({Key key, this.title}) : super(key: key);

  final String title;

  Editar_perfilState createState() => Editar_perfilState();
}

class Editar_perfilState extends State<Editar_perfil> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  void initState(){
    super.initState();
    this.cargar();
  }

  TextEditingController mostrar_nombre = new TextEditingController();
  TextEditingController mostrar_apellido = new TextEditingController();
  TextEditingController mostrar_correo = new TextEditingController();
  TextEditingController mostrar_docuemnto = new TextEditingController();
  TextEditingController mostrar_direccion= new TextEditingController();
  void cargar() async{
    Conexion conex=new Conexion();
    Map response={};
    Map request={};

    final prefs = await SharedPreferences.getInstance();
    final myString = prefs.getString('id_usuario') ?? '';
    request["codasociado"]=myString;
    response = await conex.enviar_servidor("perfil", request);
    mostrar_nombre.text=response["nombres"];
    mostrar_apellido.text=response["apellidos"];
    mostrar_correo.text=response["email"];
    mostrar_docuemnto.text=response["dni"];
    mostrar_direccion.text=response["direccion"];


  }

  void guardar() async{
    Conexion conex=new Conexion();
    Map response={};
    Map request={};
    if (_fbKey.currentState.saveAndValidate()) {
      final prefs = await SharedPreferences.getInstance();
      final myString = prefs.getString('id_usuario') ?? '';
      var datos=_fbKey.currentState.value;
      request["nombres"]=datos["nombres"];
      request["apellidos"]=datos["apellidos"];
      request["email"]=datos["email"];
      request["dni"]=datos["dni"];
      request["direccion"]=datos["direccion"];

      //  request["codasociado"] = myString;

      request["codasociado"]=myString;
      print(request);
      response = await conex.enviar_servidor("perfil_guardar", request);
      print(response);

      Notificacion n=new Notificacion();

      n.showInSnackBar_correcto("Se Actualizo Correctamente", context, _scaffoldKey);

      Timer(Duration(seconds: 1),(){

       Navigator.of(context).pop();
      });

    }

  }
  var _scaffoldKey = new GlobalKey<ScaffoldState>();
  Widget build(BuildContext context) {
    return Scaffold(
    key: _scaffoldKey,
      appBar: AppBar(
        title: Text("Editar Perfil"),
        backgroundColor: Colors.green,
      ),
      body: Container(
    height: MediaQuery.of(context).size.height,
    child: new ListView(

    children: <Widget>[


      FormBuilder(
        key: _fbKey,
        child: Column(
          children: <Widget>[
            Container(
              height: 10,
            ),
            Padding(
              child: FormBuilderTextField(

                attribute: "nombres",
                controller: mostrar_nombre,
                decoration: InputDecoration(labelText: "Nombre Completo",
                  contentPadding: EdgeInsets.fromLTRB(10, 20, 10, 20),

                  prefixIcon: Icon(Icons.person),

                  border: OutlineInputBorder(),
                ),

              ),
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            ),
            Padding(
              child: FormBuilderTextField(
                controller: mostrar_apellido,

                attribute: "apellidos",
                decoration: InputDecoration(labelText: "Apellidos Completo",
                  contentPadding: EdgeInsets.fromLTRB(10, 20, 10, 20),
                  prefixIcon: Icon(Icons.person),

                  border: OutlineInputBorder(),
                ),

              ),
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            ),
            Padding(
              child: FormBuilderTextField(
                controller: mostrar_correo,

                attribute: "email",
                decoration: InputDecoration(labelText: "Correo Electronico",
                  contentPadding: EdgeInsets.fromLTRB(10,10, 10, 10),
                  prefixIcon: Icon(Icons.email),

                  border: OutlineInputBorder(),
                ),

              ),
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            ),
            Padding(
              child: FormBuilderTextField(

                attribute: "dni",
                controller: mostrar_docuemnto,
                keyboardType: TextInputType.number,
                maxLength: 8,
                decoration: InputDecoration(labelText: "Documento",
                  counterText: '',
                  contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                  prefixIcon: Icon(Icons.domain),

                  border: OutlineInputBorder(),
                ),

              ),
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            ),

            Padding(
              child: FormBuilderTextField(
                controller: mostrar_direccion,

                attribute: "direccion",

                decoration: InputDecoration(labelText: "Direccion",
                  contentPadding: EdgeInsets.fromLTRB(10, 20, 10, 20),
                  prefixIcon: Icon(Icons.store),

                  border: OutlineInputBorder(),
                ),

              ),
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            ),
         /*   Padding(
              child: FormBuilderTextField(
                obscureText: true,
                attribute: "telefono",
                decoration: InputDecoration(labelText: "TELEFONO",
                  contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                  prefixIcon: Icon(Icons.domain),

                  border: OutlineInputBorder(),
                ),

              ),
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            ),*/

            Padding(
              padding: EdgeInsets.fromLTRB(10, 20, 10, 0),
              child: RaisedButton(onPressed: (){
                guardar();
              }
                ,color: Colors.green
                ,child: SizedBox(

                    width: MediaQuery.of(context).size.height,
                    height: 50,
                    child: Align(
                      child: Text("Actualizar",style: TextStyle(
                          color: Colors.white,
                          fontSize: 18
                      ),),
                      alignment: Alignment.center,

                    )

                ),
              ),
            ),

          ],
        ),
      ),
    ]
    )
    )
    );
  }
}