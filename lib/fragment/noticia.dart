import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:badges/badges.dart';
import 'package:carousel_slider/carousel_slider.dart';
import './../clase/noticia.dart';
import './../conexion/servidor.dart';
class Noticia extends StatefulWidget {
  Noticia({Key key, this.title}) : super(key: key);

  final String title;

  NoticiaState createState() => NoticiaState();
}

class NoticiaState extends State<Noticia> {
  bool estado_carga=false;
  void initState(){
     super.initState();
     this.mostrar();


  }
  List<Noticias> lista=[];
  void mostrar() async{
     // lista.add(Noticias("1","IX ENCUENTRO CULTURAL Y DE TURISMO SOSTENIBLE","DESCRICPCION JIMMY DASDASM FASDASJD AJSD","assets/prueba.jpg","2019-12-03"));
      //lista.add(Noticias("1","TITULO","DESCRICPCION JIMMY DASDASMFA SDAS JDAJSD","assets/prueba.jpg","2019-12-03"));
      Conexion conectar=new Conexion();
      Map request={};

      List response=await conectar.enviar_servidor("noticias", request);


    print(response);
    response.forEach((item) {
        lista.add(Noticias(item["codnoticia"],item["titulo"],item["descripcion"],item["ruta"],item["fecha"]));
    });



     setState(() {
       lista;  estado_carga=true;
     });
  }



  CarouselSlider getFullScreenCarousel(BuildContext mediaContext) {
    return CarouselSlider(
      autoPlay: true,
      enableInfiniteScroll: false,
     height: 440,
      items: lista.map(
            (url) {
          return Stack(
            children: <Widget>[
              Container(
                height: 600,
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(0.0)),
                  child: Image.network(
                   url.foto,
                    fit: BoxFit.cover,
                    width: 270.0,
                  ),
                ),
              ),
             /* Positioned(
                bottom: 20.0,
                left: 10.0,
                right: 10.0,
                child: Card(
                  elevation: 8.0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Text(
                          url.titulo,
                          style: TextStyle(
                            fontSize: 14.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(10, 0, 10, 4),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              url.descripcion,
                              textAlign: TextAlign.justify,
                              style: TextStyle(
                                fontSize: 13.0,

                              ),),
                           Container(
                             height: 6,
                           ),
                            Text("Fecha: "+url.fecha,
                              style: TextStyle(
                                fontSize: 10.0,
                                fontWeight: FontWeight.bold,
                              ),
                            )
                          ],
                        )
                      ),
                    ],
                  ),
                ),
              ),*/
            ],
          );
        },
      ).toList(),
    );
  }
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,

      children: <Widget>[

        !estado_carga?Center(
          child: Container(
            width: 40,
            height: 40,
            child:  CircularProgressIndicator(

            ),
          )
        ):
        Padding(
            padding: EdgeInsets.only(top: 15.0),
            //Builder needed to provide mediaQuery context from material app
            child: Builder(builder: (context) {
              return Column(children: [


                getFullScreenCarousel(context),
              ]);
            })),



      ],

    );
  }
}

