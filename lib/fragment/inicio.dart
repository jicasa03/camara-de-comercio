import 'package:camara/conexion/servidor.dart';
import 'package:flutter/material.dart';
import 'package:badges/badges.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import '../servicio.dart';
import 'package:shared_preferences/shared_preferences.dart';


import 'package:sign_button/sign_button.dart';

import 'package:flutter_awesome_buttons/flutter_awesome_buttons.dart';
import 'package:url_launcher/url_launcher.dart';
class Inicio extends StatefulWidget {
  Inicio({Key key, this.title}) : super(key: key);

  final String title;

  InicioState createState() => InicioState();
}

class InicioState extends State<Inicio> {


  void initState(){

    super.initState();
    this.mostrar();
  }

 bool estado=true;

  String nombre="";
  final _formKey = GlobalKey<FormBuilderState>();

  void mostrar() async{


    Map request={};

    final prefs = await SharedPreferences.getInstance();

    final nombres=prefs.getString('nombre')??'';
    estado=prefs.getBool('estado_notificacion')??true;
    if(_formKey.currentState.saveAndValidate()){
      _formKey.currentState.fields["notificacion"].currentState.didChange(estado);
    }


    setState(() {

      nombre=nombres;
    });
  }

  void actualizarnotificacion(bool esta) async
  {
    final prefs = await SharedPreferences.getInstance();
     prefs.setBool('estado_notificacion',esta);
     int i=0;
     if(esta)
       {
         i=1;
       }
    Map request={};
    final myString = prefs.getString('id_usuario') ?? '';
    request["codasociado"]=myString;
    request["notificar"]=i;
    var list= Conexion().enviar_servidor("notificar_asociado", request);
    print(request);
     
  }
  Widget build(BuildContext context) {
    return Column(
    mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          height: 130,
          width: 300,
          child:  Image.asset("assets/logo.png"),
        ),
        Container(
          height: 20,
        ),
        Text("Bienvenido "+nombre,style: TextStyle(
          fontSize: 15,
          fontWeight: FontWeight.bold
        ),),
        Container(
          height: 30,
        ),
        Padding(padding: EdgeInsets.fromLTRB(20, 0, 20, 0)
          ,child:   Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              SignInButton.mini(

                buttonType: ButtonType.twitter,
                onPressed: () {
                  _launchURL('https://twitter.com/CCPTSM');
                },
              ),
              SignInButton.mini(

                buttonType: ButtonType.facebook,
                onPressed: () {
                  _launchURL('https://www.facebook.com/Camaratarapoto');
                },
              ),
              InstagramButton(onPressed: (){
                _launchURL('https://www.instagram.com/camaratarapoto/?hl=es-la');
              },),

              /*    SignInButton.mini(
             buttonType: ButtonType.,
             onPressed: () {},
           ),*/
              SignInButton.mini(

                buttonType: ButtonType.linkedin,
                onPressed: () {
                  _launchURL('https://www.linkedin.com/in/c%C3%A1mara-de-comercio-tarapoto-ccptsmt-4a0813a9/');
                },
              ),
            ],
          ),
        ),
       FormBuilder(
           key: _formKey,
           child:

       Column(
         children: <Widget>[
             Padding(padding:EdgeInsets.fromLTRB(30, 0, 30, 0),
             child: FormBuilderSwitch(attribute: "notificacion", label: Text(
                 "Activar notificaciones"
             ),
               decoration: InputDecoration(
                   border: InputBorder.none
               ),
               onChanged: (value)
               {
                 print(value);
                 actualizarnotificacion(value);
               },
             ),
             )
         ],
       )),

        Padding(padding: EdgeInsets.fromLTRB(20, 20, 20, 10)
          ,child: RaisedButton(
            onPressed: (){


              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => Servicio(),
                  // Pass the arguments as part of the RouteSettings. The
                  // ExtractArgumentScreen reads the arguments from these
                  // settings.

                ),
              );
            },
            color: Colors.green,
            child: SizedBox(
              height: 50,
              width: MediaQuery.of(context).size.width,
              child: Align(
                alignment: Alignment.center,
                child: Text("Servicios",style: TextStyle(fontWeight: FontWeight.bold,color: Colors.white),),
              ),
            ),
          ),

        )



      ],
    );
  }

  _launchURL(String url) async {
    //const url = 'https://flutter.dev';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}

class CustomIcons {
  static const IconData twitter = IconData(0xe900, fontFamily: "CustomIcons");
  static const IconData facebook = IconData(0xe901, fontFamily: "CustomIcons");
  static const IconData googlePlus =
  IconData(0xe902, fontFamily: "CustomIcons");
  static const IconData linkedin = IconData(0xe903, fontFamily: "CustomIcons");
}

class SocialIcon extends StatelessWidget {
  final List<Color> colors;
  final IconData iconData;
  final Function onPressed;
  SocialIcon({this.colors, this.iconData, this.onPressed});
  @override
  Widget build(BuildContext context) {
    return new Padding(
      padding: EdgeInsets.only(left: 14.0),
      child: Container(
        width: 45.0,
        height: 45.0,
        decoration: BoxDecoration(
            shape: BoxShape.circle,
          //  gradient: LinearGradient(colors: colors,tileMode: TileMode.clamp)
            ),
        child: RawMaterialButton(
          shape: CircleBorder(),
          onPressed: onPressed,
          child: Icon(iconData, color: Colors.white),
        ),
      ),
    );
  }
}