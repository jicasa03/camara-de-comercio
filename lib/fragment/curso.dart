import 'package:camara/clase/curso.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import './../clase/eventos.dart';
import 'package:shared_preferences/shared_preferences.dart';
import './../clase/eventos.dart';
import 'package:progress_dialog/progress_dialog.dart';
import './../conexion/servidor.dart';
import 'package:url_launcher/url_launcher.dart';
class Curso extends StatefulWidget {
  Curso({Key key, this.title}) : super(key: key);

  final String title;

  CursoState createState() => CursoState();
}

class CursoState extends State<Curso> {

  bool estado_carga=false;
  List<Cursos> lista=[];
  void initState(){
    super.initState();
    this.mostrar();


  }

  void mostrar() async{
    // lista.add(Noticias("1","IX ENCUENTRO CULTURAL Y DE TURISMO SOSTENIBLE","DESCRICPCION JIMMY DASDASM FASDASJD AJSD","assets/prueba.jpg","2019-12-03"));
    //lista.add(Noticias("1","TITULO","DESCRICPCION JIMMY DASDASMFA SDAS JDAJSD","assets/prueba.jpg","2019-12-03"));
    Conexion conectar=new Conexion();
    Map request={};
    final prefs = await SharedPreferences.getInstance();
    final myString = prefs.getString('id_usuario') ?? '';
    request["codasociado"]=myString;
    List response=await conectar.enviar_servidor("cursos", request);


    print(response);
    response.forEach((item) {
      lista.add(Cursos(
        id: item["codactividad"],
          titulo:  item["titulo"],
          descripcion:item["descripcion"],
          foto:item["ruta"],
          fechainicio:item["fechaactividad"],
          tipo:item["tipo"],
          asistencia:0,//int.parse(item["asiste"]),
          horainicio:item["horainicio"],
        costo:item["costo"] ,
        fechafin: item["fechainicio"],
        horafin:item["horafin"],
        zoom: item["zoom"],
          ));
    });



    setState(() {
      lista;
      estado_carga=true;
    });
  }


  _launchURL(String url) async {
    //const url = 'https://flutter.dev';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
  ProgressDialog pr;
  void elegir_asistencia(int cod,int verificar) async{
    print(cod);
    pr = new ProgressDialog(context);
    pr.style(
        message: 'Procesando...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600)
    );

    pr.show();

    Conexion conectar=new Conexion();
    Map request={};
    final prefs = await SharedPreferences.getInstance();
    final myString = prefs.getString('id_usuario') ?? '';
    request["codasociado"]=myString;
    Map response;
    request["codactividad"]=cod;
    if(verificar==1){
      response =await conectar.enviar_servidor("asistir", request);
    }else {
      response =await conectar.enviar_servidor("noasistir", request);

    }
    // print(response);
    lista=[];
   // lista_=[];
    await this. mostrar();
    pr.hide();
  }



  final List<String> imgList = [
    'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80',
    'https://images.unsplash.com/photo-1522205408450-add114ad53fe?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=368f45b0888aeb0b7b08e3a1084d3ede&auto=format&fit=crop&w=1950&q=80',
    'https://images.unsplash.com/photo-1519125323398-675f0ddb6308?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=94a1e718d89ca60a6337a6008341ca50&auto=format&fit=crop&w=1950&q=80',
    'https://images.unsplash.com/photo-1523205771623-e0faa4d2813d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=89719a0d55dd05e2deae4120227e6efc&auto=format&fit=crop&w=1953&q=80',
    'https://images.unsplash.com/photo-1508704019882-f9cf40e475b4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=8c6e5e3aba713b17aa1fe71ab4f0ae5b&auto=format&fit=crop&w=1352&q=80',
    'https://images.unsplash.com/photo-1519985176271-adb1088fa94c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=a0c8d632e977f94e5d312d9893258f59&auto=format&fit=crop&w=1355&q=80'
  ];





  CarouselSlider getFullScreenCarousel(BuildContext mediaContext) {
    return CarouselSlider(
      autoPlay: false,
      scrollDirection: Axis.vertical,
      enableInfiniteScroll: false,
      initialPage: 0,
      height:MediaQuery.of(context).size.height,
      aspectRatio: 5,





      items: lista.map(
            (url) {
          return
                          Column(

                            children: <Widget>[
                              Image.network(
                                url.foto,

                                width: MediaQuery.of(context).size.width,
                                height: 150,

                              ),

                              Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Text(
                                  url.titulo,
                                  style: TextStyle(
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              Text(
                                "DESCRIPCION DE LA ACTIVIDAD",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 12
                                ),
                              ),
                              Text(
                                url.descripcion,
                                style: TextStyle(

                                    fontSize: 12
                                ),
                              ),


                              Text("FECHA ACTIVIDAD INICIO/FIN: ",
                                style: TextStyle(
                                  fontSize: 12.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Text(url.fechainicio+"/"+url.fechafin,
                                style: TextStyle(
                                  fontSize: 12.0,

                                ),
                              ),
                              Text("HORA ACTIVIDAD INICIO/FIN: ",
                                style: TextStyle(
                                  fontSize: 12.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Text(url.horainicio+"/"+url.horafin,
                                style: TextStyle(
                                  fontSize: 12.0,

                                ),
                              ),



                              Text("S/ COSTO ACTIVIDAD",
                                style: TextStyle(
                                  fontSize: 12.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Text("S/ "+url.costo,
                                style: TextStyle(
                                  fontSize: 12.0,

                                ),
                              ),
                              Text("LINK ZOOM",
                                style: TextStyle(
                                  fontSize: 12.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                             GestureDetector(
                               onTap: (){
                                 _launchURL(url.zoom);
                               },
                               child:  Text(url.zoom,
                                 style: TextStyle(
                                   fontSize: 12.0,

                                 ),
                               ),
                             )




                               ,Container(
                   height: 0,
              ),
                              Divider(

                                color: Colors.black,

                              )

                            ],
                          );









        },
      ).toList(),
    );
  }
  Widget build(BuildContext context) {
   return Column(
     crossAxisAlignment: CrossAxisAlignment.center,
     mainAxisAlignment: MainAxisAlignment.start,

     children: <Widget>[



       !estado_carga?Center(
         child: Container(
           width: 40,
           height: 40,
           child:  CircularProgressIndicator(

           ),
         ),
       ):
       Padding(
           padding: EdgeInsets.only(top: 0.0),
           //Builder needed to provide mediaQuery context from material app
           child: Builder(builder: (context) {
             return Column(
                 crossAxisAlignment: CrossAxisAlignment.center,
                 mainAxisAlignment: MainAxisAlignment.start,
                 children: [


               getFullScreenCarousel(context),
             ]);
           })),




     ],

   );
  }
}