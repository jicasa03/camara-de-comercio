
import 'package:flutter/material.dart';
import 'package:badges/badges.dart';
import '../conexion/servidor.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../ui/notificacion.dart';
import '../provider/Notificaciones.dart';
import 'package:provider/provider.dart';
class Notificacion extends StatefulWidget {
   int notificar;
  Notificacion({Key key,this.notificar}) : super(key: key);



  NotificacionState createState() => NotificacionState();
}

class NotificacionState extends State<Notificacion> {
  int notificacion=0;
  void initState(){

    super.initState();
  this.mostrar();
    notificacion=widget.notificar;
  }
  List response=[];
  void mostrar() async{
    Conexion conex=new Conexion();

    Map request={};

    final prefs = await SharedPreferences.getInstance();
    final myString = prefs.getString('id_usuario') ?? '';
    request["codasociado"]=myString;
    response = await conex.enviar_servidor("notificaciones", request);

    print(response);
    setState(() {
      response;
    });
  }
  void mostrar_notificacion(String idnotificacion,int index) async
  {
    Conexion conex=new Conexion();

    Map request={};
    response[index]["visto"]=1;
    setState(() {

    });
    final prefs = await SharedPreferences.getInstance();
    final myString = prefs.getString('id_usuario') ?? '';
    request["codasociado"]=myString;
    request["codmensaje"]=idnotificacion;
   //   await conex.enviar_servidor("notificaciones_registrar_vista", request);

    int dato=0;
      conex.enviar_servidor("notificaciones_registrar_vista", request).then((value) {
        conex.enviar_servidor("notificaciones_novistas", request).then((value) {
          // print(response);
          int tam=value;

          context.read<Counter>().inicio(tam);
        });
      });




    //print(response);
  }






  Widget build(BuildContext context) {
     return ListView.builder(
         itemCount:response.length ,
         itemBuilder: (context, position) {

       return   ListTile(
               hoverColor: Colors.green,
         tileColor:response[position]["visto"]==0?Colors.green:null,
         onTap: (){
          mostrar_notificacion(response[position]["codmensaje"],position);
         },
         leading: Container(
             width: 50.0,
             height: 50.0,
             decoration: new BoxDecoration(
                 shape: BoxShape.circle,
                 image: new DecorationImage(
                     fit: BoxFit.fill,
                     image: new AssetImage(
                         "assets/default.png")
                 )
             )),
         title: Text(response[position]["descripcion"]),
         subtitle: Text("Fecha: "+response[position]["fecha"]),

       );
     });
  }
}