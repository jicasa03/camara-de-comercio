import 'package:camara/clase/curso.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import './../conexion/servidor.dart';
import './../clase/eventos.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
class Evento extends StatefulWidget {
  Evento({Key key, this.title}) : super(key: key);

  final String title;

  EventoState createState() =>EventoState();
}

class EventoState extends State<Evento> {
  List<Eventos> lista=[];
  void initState(){
    super.initState();
    this.mostrar();


  }
  bool estado_carga=false;
  List<Cursos> lista_=[];
  void mostrar() async{
    // lista.add(Noticias("1","IX ENCUENTRO CULTURAL Y DE TURISMO SOSTENIBLE","DESCRICPCION JIMMY DASDASM FASDASJD AJSD","assets/prueba.jpg","2019-12-03"));
    //lista.add(Noticias("1","TITULO","DESCRICPCION JIMMY DASDASMFA SDAS JDAJSD","assets/prueba.jpg","2019-12-03"));
    Conexion conectar=new Conexion();
    Map request={};
    final prefs = await SharedPreferences.getInstance();
    final myString = prefs.getString('id_usuario') ?? '';
    request["codasociado"]=myString;
    List response=await conectar.enviar_servidor("actividades", request);

    lista_=[];
    print(response);
    response.forEach((item) {
      lista.add(Eventos(item["codactividad"],item["titulo"],item["descripcion"],item["ruta"],item["fecha"],item["tipo"],int.parse(item["asiste"])));
      lista_.add(Cursos(
        id: item["codactividad"],
        titulo:  item["titulo"],
        descripcion:item["descripcion"],
        foto:item["ruta"],
        fechainicio:item["fechaactividad"],
        tipo:item["tipo"],
        asistencia:int.parse(item["asiste"]),
        horainicio:item["horainicio"],
        costo:item["costo"] ,
        fechafin: item["fechainicio"],
        horafin:item["horafin"],
        zoom: item["zoom"],
      ));

    });



    setState(() {
      lista;
      estado_carga=true;lista_;
    });
  }
  ProgressDialog pr;
  void elegir_asistencia(int cod,int verificar) async{
    print(cod);
    pr = new ProgressDialog(context);
    pr.style(
        message: 'Procesando...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600)
    );

    pr.show();

    Conexion conectar=new Conexion();
    Map request={};
    final prefs = await SharedPreferences.getInstance();
    final myString = prefs.getString('id_usuario') ?? '';
    request["codasociado"]=myString;
    Map response;
    request["codactividad"]=cod;
    if(verificar==1){
      response =await conectar.enviar_servidor("asistir", request);
    }else {
      response =await conectar.enviar_servidor("noasistir", request);

    }
   // print(response);
    lista=[];
    lista_=[];
   await this. mostrar();
    pr.hide();
  }
  
  
  
  CarouselSlider getFullScreenCarousel(BuildContext mediaContext) {
    return CarouselSlider(
      autoPlay: false,
      scrollDirection: Axis.vertical,
      enableInfiniteScroll: false,
      initialPage: 0,
      height: MediaQuery.of(context).size.height+100,
      items: lista_.map(
            (url) {
          return       Column(

            children: <Widget>[
              Image.network(
                url.foto,

                width: MediaQuery.of(context).size.width,
                height: 150,

              ),

              Padding(
                padding: const EdgeInsets.all(5.0),
                child: Text(
                  url.titulo,
                  style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Text(
                "DESCRIPCION DE LA ACTIVIDAD",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 12
                ),
              ),

              
              Container(
                width: MediaQuery.of(context).size.width,
                height: 50,
                child: Text(

                  url.descripcion,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(

                      fontSize: 12
                  ),
                ),
              ),


              Text("FECHA ACTIVIDAD INICIO/FIN: ",
                style: TextStyle(
                  fontSize: 12.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(url.fechainicio+"/"+url.fechafin,
                style: TextStyle(
                  fontSize: 12.0,

                ),
              ),
              Text("HORA ACTIVIDAD INICIO/FIN: ",
                style: TextStyle(
                  fontSize: 12.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(url.horainicio+"/"+url.horafin,
                style: TextStyle(
                  fontSize: 12.0,

                ),
              ),



              Text("S/ COSTO ACTIVIDAD",
                style: TextStyle(
                  fontSize: 12.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text("S/ "+url.costo,
                style: TextStyle(
                  fontSize: 12.0,

                ),
              ),
              Text("LINK ZOOM",
                style: TextStyle(
                  fontSize: 12.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              GestureDetector(
                onTap: (){
                  _launchURL(url.zoom);
                },
                child:  Text(url.zoom,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 12.0,

                  ),
                ),
              ),

              (url.asistencia==0)?Padding(padding: EdgeInsets.fromLTRB(10, 0, 10, 0)
                ,child: RaisedButton(onPressed: (){

                  elegir_asistencia(int.parse(url.id),1);
                },
                  color: Colors.blue,
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width-100,
                    height: 20,

                    child: Center(
                      child: Text("ASISTIR",style:TextStyle(
                          color: Colors.white
                      ),),
                    ),
                  ),
                ),
              ):Padding(padding: EdgeInsets.fromLTRB(10, 0, 10, 0)
                ,child: OutlineButton(onPressed: (){

                  elegir_asistencia(int.parse(url.id),2);
                },
                  color: Colors.blue,
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width-100,
                    height: 20,

                    child: Center(
                      child: Text("CANCELAR",style:TextStyle(
                          color: Colors.black
                      ),),
                    ),
                  ),
                ),
              ),


              Divider(

                color: Colors.black,

              ),


            ]


          );





        },
      ).toList(),
    );
  }
  _launchURL(String url) async {
    //const url = 'https://flutter.dev';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,

      children: <Widget>[



        !estado_carga?Center(
          child: Container(
            width: 40,
            height: 40,
            child:  CircularProgressIndicator(

            ),
          ),
        ):
        Padding(
            padding: EdgeInsets.only(top: 0.0),
            //Builder needed to provide mediaQuery context from material app
            child: Builder(builder: (context) {
              return Container(
                child: SingleChildScrollView(
                  child: Column(children: [


                    getFullScreenCarousel(context),
                 /*   Container(height:200,
                    width: 10,
                      color: Colors.black,
                    )*/

                  ])
                ),
              );
            })),




      ],

    );
  }
}