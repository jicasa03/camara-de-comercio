import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import './conexion/servidor.dart';
import './ui/notificacion.dart';
import 'dart:convert';
class Registrar extends StatefulWidget {
  Registrar({Key key, this.title}) : super(key: key);

  final String title;

  RegistrarState createState() => RegistrarState();
}

class RegistrarState extends State<Registrar> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  String boton="REGISTRAR";

  bool estado=true;
  List<DropdownMenuItem> lista_menu=[];
  void initState()
  {
    super.initState();
    this.cargar_tipo();
  }
  void cargar_tipo() async
  {
    Map request={};
    Conexion conexion=new Conexion();
    List lista=await conexion.enviar_servidor("grupos", request);
    print(lista);
    lista.forEach((element) {
      lista_menu.add(new DropdownMenuItem(
        value: element["codgrupo"],
        child: Text( element["descripcion"]),
      ));
    });

    setState(() {

    });
  }
  void registrar() async{
    Notificacion notificar=new Notificacion();
    Map request={};

    if (_fbKey.currentState.saveAndValidate()) {

      setState(() {
        boton="CARGANDO...";
        //estado=false;
      });
      request=_fbKey.currentState.value;
      var body = json.encode(request);
      print(body);
      //request["clave"]="123";

      Map response={};
      response =await Conexion().enviar_servidor("crear_usuario", request);
      print(response);
      if(response["estado"]==1){
        notificar.showInSnackBar_correcto(response["mensaje"], context, _scaffoldKey);
        Navigator.pop(context);

      }else{
        print("errror");

        notificar.showInSnackBar_error(response["mensaje"], context, _scaffoldKey);
        setState(() {
          boton="REGISTRAR";
          estado=true;
        });
      }


    }

  }

  var _scaffoldKey = new GlobalKey<ScaffoldState>();

  Widget build(BuildContext context) {
    return Scaffold(
key: _scaffoldKey,
   appBar: AppBar(
     title: Text("Registrar Cliente"),
     backgroundColor: Colors.green,
   ),
      body:
    new Container(
    child:SingleChildScrollView(
    child: Column(

    children: <Widget>[

      FormBuilder(
        key: _fbKey,
        child: Column(
          children: <Widget>[
            Container(
              height: 10,
            ),
            Padding(
              child: FormBuilderTextField(

                attribute: "dni",
                keyboardType: TextInputType.number,
                maxLength: 8,
                decoration: InputDecoration(labelText: "DNI",
                  contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                  prefixIcon: Icon(Icons.credit_card),
                  counterText: '',
                  border: OutlineInputBorder(),
                ),

              ),
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            ),
            Padding(
              child: FormBuilderTextField(

                attribute: "nombres",
                decoration: InputDecoration(labelText: "Nombre Completo",
                  contentPadding: EdgeInsets.fromLTRB(10, 20, 10, 20),
                  prefixIcon: Icon(Icons.person),

                  border: OutlineInputBorder(),
                ),

              ),
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            ),
            Padding(
              child: FormBuilderTextField(

                attribute: "apellidos",
                decoration: InputDecoration(labelText: "Apellido Completo",
                  contentPadding: EdgeInsets.fromLTRB(10, 20, 10, 20),
                  prefixIcon: Icon(Icons.person_pin),

                  border: OutlineInputBorder(),
                ),

              ),
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            ),
            Padding(
              child: FormBuilderTextField(

                attribute: "direccion",
                decoration: InputDecoration(labelText: "DIRECCIÓN",
                  contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                  prefixIcon: Icon(Icons.domain),

                  border: OutlineInputBorder(),
                ),

              ),
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            ),
            Padding(
              child: FormBuilderTextField(

                attribute: "email",
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(labelText: "Correo Electronico",
                  contentPadding: EdgeInsets.fromLTRB(10,10, 10, 10),
                  prefixIcon: Icon(Icons.email),

                  border: OutlineInputBorder(),
                ),

              ),
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            ),
            Padding(
              child: FormBuilderTextField(
                validators: [
                  FormBuilderValidators.required()
                ],
                attribute: "celular",

                keyboardType: TextInputType.number,
                decoration: InputDecoration(labelText: "Celular",
                  contentPadding: EdgeInsets.fromLTRB(10,10, 10, 10),
                  prefixIcon: Icon(Icons.phone_android),

                  border: OutlineInputBorder(),
                ),

              ),
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            ),

            Padding(
              child: FormBuilderTextField(

                attribute: "codigo",

                decoration: InputDecoration(labelText: "CODIGO DE USUARIO",
                  contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                  prefixIcon: Icon(Icons.person),

                  border: OutlineInputBorder(),
                ),

              ),
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            ),

            Padding(
              child: FormBuilderTextField(

                attribute: "clave",
                decoration: InputDecoration(labelText: "CLAVE",
                  contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                  prefixIcon: Icon(Icons.security),

                  border: OutlineInputBorder(),
                ),

              ),
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            ),

            Padding(
              child: FormBuilderTextField(

                attribute: "ruc_empresa",
                decoration: InputDecoration(labelText: "RUC EMPRESA",
                  contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                  prefixIcon: Icon(Icons.person),

                  border: OutlineInputBorder(),
                ),

              ),
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            ),
            Padding(
              child: FormBuilderTextField(

                attribute: "razonsocial_empresa",
                decoration: InputDecoration(labelText: "RAZÓN SOCIAL",
                  contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                  prefixIcon: Icon(Icons.person),

                  border: OutlineInputBorder(),
                ),

              ),
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            ),
            Padding(
              child: FormBuilderTextField(

                attribute: "direccion_empresa",
                decoration: InputDecoration(labelText: "DIRECCIÓN EMPRESA",
                  contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                  prefixIcon: Icon(Icons.person),

                  border: OutlineInputBorder(),
                ),

              ),
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            ),

            Padding(
              child:   FormBuilderDropdown(

                attribute: "codgrupo",
                items:lista_menu,
                decoration: InputDecoration(labelText: "GRUPO O GREMIO",
                  contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                  prefixIcon: Icon(Icons.person),

                  border: OutlineInputBorder(),
                ),

              ),
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(10, 20, 10, 0),
              child: RaisedButton(onPressed:estado?registrar:null,
                color: Colors.green
                ,child: SizedBox(

                    width: MediaQuery.of(context).size.height,
                    height: 50,
                    child: Align(
                      child: Text("$boton",style: TextStyle(
                          color: Colors.white,
                          fontSize: 18
                      ),),
                      alignment: Alignment.center,

                    )

                ),
              ),
            ),

          ],
        ),
      )  ],
    ),
    )
    )
    );
  }
}