import 'dart:convert';

import 'package:camara/conexion/servidor.dart';

import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:shared_preferences/shared_preferences.dart';
import './ui/notificacion.dart';
class Nuevo_servicio extends StatefulWidget {
  Nuevo_servicio({Key key, this.data}) : super(key: key);

  final Map data;

  Nuevo_servicioState createState() => Nuevo_servicioState();
}

class Nuevo_servicioState extends State<Nuevo_servicio> {

  void initState()
  {
    super.initState();
    this.mostrar_info();
    //this.cargar();
    print("prueba");
  }
  List response=[];

  String titulo="";
  String descripcion="";
  void cargar() async
  {
    Conexion conexion=new Conexion();
    Map request={};
    response= await conexion.enviar_servidor("servicios", request);
    response.forEach((element) {
      print(element);
    });

    setState(() {

    });
  }

  void mostrar_info() async
  {
     String codservicio=  widget.data["codservicio"];
     Map request={};
     response= await Conexion().enviar_servidor("servicio_info/"+codservicio, request);
     titulo=response[0]["titulo"];
     descripcion=response[0]["descripcion"];
     setState(() {

     });
   /*  response.forEach((element) {
       print(element);
     });*/
  }

  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();

  Widget build(BuildContext context) {
    return Scaffold(
      key:_scaffoldKey,
        appBar: AppBar(
          title: Text("Servicio"),
          backgroundColor: Colors.green,

        ),
        body:
         Container(
           child: SingleChildScrollView(
             child: Column(
               children: <Widget>[
                 Center(
                   child: Text(titulo,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20
                    ),
                   ),
                 ),
                 Container(
                   child: Text(descripcion,
                     textAlign: TextAlign.justify,
                     style: TextStyle(

                   ),),
                 )

               ],
             ),
           ),
         )

    );
        /*Container(
          child: SingleChildScrollView(

            child: FormBuilder(
              key: _fbKey,
              child: Column(
               children: [
                 Padding(padding: EdgeInsets.fromLTRB(10, 30, 10, 10),
                 child: FormBuilderTextField(
                   attribute: "titulo",
                   decoration: InputDecoration(labelText: "Titulo",
                     contentPadding: EdgeInsets.fromLTRB(10, 20, 10, 20),


                     border: OutlineInputBorder(),
                   ),
                 ),
                 ),

                 Padding(padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                   child: FormBuilderTextField(
                     attribute: "descripcion",
                     maxLines: 4,
                     decoration: InputDecoration(labelText: "Descripción",
                       contentPadding: EdgeInsets.fromLTRB(10, 20, 10, 20),


                       border: OutlineInputBorder(),
                     ),
                   ),
                 ),
                 Padding(
                   padding: EdgeInsets.fromLTRB(10, 20, 10, 0),
                   child: RaisedButton(onPressed:estado?registrar:null,
                     color: Colors.green
                     ,child: SizedBox(

                         width: MediaQuery.of(context).size.height,
                         height: 50,
                         child: Align(
                           child: Text("$boton",style: TextStyle(
                               color: Colors.white,
                               fontSize: 18
                           ),),
                           alignment: Alignment.center,

                         )

                     ),
                   ),
                 ),

               ],
              ),
            ),


          ),
        )

    );*/
  }
  var _scaffoldKey = new GlobalKey<ScaffoldState>();
 bool estado=true;
 String boton="REGISTRAR";
  void registrar() async{
    Notificacion notificar=new Notificacion();
    Map request={};

    if (_fbKey.currentState.saveAndValidate()) {

      setState(() {
        boton="CARGANDO...";
        //estado=false;
      });
      final prefs = await SharedPreferences.getInstance();
      request=_fbKey.currentState.value;
      //request["codservicio"]=widget.data["codservicio"];
      request["codservicio"]=widget.data["codservicio"];
      request["codasociado"]= prefs.getString("id_usuario");
       var body = json.encode(request);
      print(body);
      //request["clave"]="123";

      Map response={};
      response =await Conexion().enviar_servidor("servicio_guardar", request);
      print(response);
      if(response["estado"]==1){
        notificar.showInSnackBar_correcto(response["mensaje"], context, _scaffoldKey);
        Navigator.pop(context);

      }else{
        print("errror");

        notificar.showInSnackBar_error(response["mensaje"], context, _scaffoldKey);
        setState(() {
          boton="REGISTRAR";
          estado=true;
        });
      }


    }

  }
}

