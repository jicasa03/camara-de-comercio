import 'package:flutter/material.dart';
import './fragment/inicio.dart';
import 'package:badges/badges.dart';
import './fragment/notificacion.dart';
import './fragment/curso.dart';
import './fragment/noticia.dart';
import './fragment/evento.dart';
import './main.dart';
import './editar_perfil.dart';
import './conexion/servidor.dart';
import 'package:shared_preferences/shared_preferences.dart';
import './provider/Notificaciones.dart';
import 'package:provider/provider.dart';
class Principal extends StatefulWidget {
  Principal({Key key, this.title}) : super(key: key);

  final String title;

  PrincipalState createState() => PrincipalState();

}

class PrincipalState extends State<Principal> {

  GlobalKey navBarGlobalKey = GlobalKey(debugLabel: 'bottomAppBar');
  Counter counter;
   int tam=0;
  final List<Widget> _children = [
   Inicio(),
    Notificacion(),
    Curso(),
    Evento(),
    Noticia()
  ];
void initState(){

  super.initState();
  this.mostrar();
}
  List response=[];

  String nombre="";
  void mostrar() async{
    Conexion conex=new Conexion();

    Map request={};

    final prefs = await SharedPreferences.getInstance();
    final myString = prefs.getString('id_usuario') ?? '';
    final nombres=prefs.getString('nombre')??'';
    request["codasociado"]=myString;
    int dato=0;
    dato = await conex.enviar_servidor("notificaciones_novistas", request);

   // print(response);
    tam=dato;

    context.read<Counter>().inicio(tam);
    setState(() {

      nombre=nombres;
    });
  }
final List<String> nombres=[
  "INICIO",
  "Notificaciones",
  "Cursos",
  "Eventos",
  "Noticias"
];
  int _cIndex = 0;



  void _incrementTab(index) {
    setState(() {
      _cIndex = index;
    });
  }


  Widget build(BuildContext context) {

    return Scaffold(

      body: _children[_cIndex], //
      appBar: AppBar(
        title: Text(nombres[_cIndex].toUpperCase()),
        backgroundColor: Colors.green,
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[

            Padding(
            padding: EdgeInsets.fromLTRB(10,10, 10,10),
              child: Image.asset("assets/logo.png",
              height: 150,
                width: 100,
              )

            ),






            ListTile(
              title: Text("Editar Perfil "),
              trailing: Icon(Icons.arrow_forward),
              onTap: (){
                Navigator.of(context).pop(false);
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Editar_perfil(),
                    // Pass the arguments as part of the RouteSettings. The
                    // ExtractArgumentScreen reads the arguments from these
                    // settings.

                  ),
                );

              },
            ),
            ListTile(
              title: Text("Salir"),
              trailing: Icon(Icons.arrow_forward),
              onTap: (){

                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MyHomePage(),
                    // Pass the arguments as part of the RouteSettings. The
                    // ExtractArgumentScreen reads the arguments from these
                    // settings.

                  ),
                );
              },
            ),
            ListTile(
              title: Text("Cancelar"),
              trailing: Icon(Icons.arrow_forward),
              onTap: (){
                Navigator.of(context).pop(false);
              },
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        key: navBarGlobalKey,
        currentIndex: _cIndex,
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.green,

        onTap: (index){
          _incrementTab(index);
        },
        // ill be set when a new tab is tapped
        items: [
          BottomNavigationBarItem(
            icon: new Icon(Icons.home,),
            title: new Text('Inicio'),


          ),
          BottomNavigationBarItem(
            icon: Badge(
          badgeContent: Text(context.watch<Counter>().count.toString(),style: TextStyle(
            color: Colors.white

          ),),
          animationType:BadgeAnimationType.slide	,
          child: Icon(Icons.notifications),
          ),
            title: new Text('Notificacion'),

          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.book),
              title: Text('Cursos')
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.event),
              title: Text('Eventos')
          )
          ,

          BottomNavigationBarItem(
              icon: Icon(Icons.new_releases),
              title: Text('Noticias')
          )
          ,
        ],
      ),
    );
  }
}

class PlaceholderWidget extends StatelessWidget {
  final Color color;

  PlaceholderWidget(this.color);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: color,
    );
  }
}